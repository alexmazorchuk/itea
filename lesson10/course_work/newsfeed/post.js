// оновлення локального сховища
const refreshLocalStorage = () => localStorage.setItem("newsPosts", JSON.stringify(postObj));

// конструктор класу новини 
class Post {
    constructor(Id, Author, Text, Image) {
        this.id = Id;
        this.author = Author;
        this.text = Text;
        this.imageUrl = Image;
        this.comments = [];
        this.likes = 0;
        this.createdAt = new Date().toLocaleString('en-GB');
    }

    AddLike() {
        this.likes++;
        let addLikes = document.querySelector(`[data-id="${this.id}"] .likes`);
        addLikes.innerText = `Likes: ${this.likes}`;
        refreshLocalStorage();
    }

    WriteComment(event) {
        let formComment = document.createElement('form');
        formComment.setAttribute('id', 'commentForm');

        let authComment = document.createElement('input');
        authComment.setAttribute('id', 'commentAuthor');
        authComment.setAttribute('placeholder', 'Author');
        authComment.setAttribute('required', 'required');
        formComment.appendChild(authComment);

        let textComment = document.createElement('textarea');
        textComment.setAttribute('id', 'commentText');
        textComment.setAttribute('placeholder', 'Comment');
        textComment.setAttribute('required', 'required');
        textComment.rows = 3;
        formComment.appendChild(textComment);

        let submitComment = document.createElement('button');
        submitComment.innerText = 'Send Post';
        submitComment.classList.add('send-btn');
        formComment.appendChild(submitComment);

        let dataId = event.target.dataset.id;
        let addForm = document.querySelector(`.item-post[data-id="${dataId}"]`);
        addForm.appendChild(formComment);

        let form = document.getElementById('commentForm');
        form.scrollIntoView();

        formComment.dataset.id = this.id;
        formComment.addEventListener('submit', this.SendComment.bind(this));
    };

    SendComment(event) {
        event.preventDefault();

        console.log('id', this.id);
        let dataId = event.target.dataset.id;
        let arrayComments = this.comments;
        let id = arrayComments.length + 1;
        let author = document.querySelector(`#commentForm[data-id="${dataId}"]>#commentAuthor`);
        let text = document.querySelector(`#commentForm[data-id="${dataId}"]>#commentText`);
        console.log('sendComment:', author.value, text.value);

        // обробляємо дані
        arrayComments.push(new Comment(id, author.value, text.value, ''));
        this.comments = arrayComments;

        refreshLocalStorage();

        // змінюємо число кількості коментарів
        let commentInfo = document.querySelector(`.comments-info[data-id="${dataId}"]`);
        commentInfo.innerText = `Comments (${arrayComments.length})`

        // чистимо форму коментаря
        let commentForm = document.querySelector(`#commentForm[data-id="${dataId}"]`);
        commentForm.childNodes.forEach(function(element) {
            // console.log('element commentForm', element);
            element.value = '';
        });

        CreatePosts();
    };
}

let newsBlock = document.getElementById('newsList');
let postObj = [];
// начинка блоку постів із сховища
const CreatePosts = () => {
    // пересортовуємо - нові вгорі
    postObj.reverse();

    // чистимо вміст блока
    newsBlock.innerHTML = null;

    postObj.forEach(function(item) {
        console.log('CreatePost', item);

        // стаття
        let listPost = document.createElement('li');
        listPost.classList.add('item-post');
        listPost.dataset.id = item.id;

        // автор
        let listAuthor = document.createElement('h2');
        listAuthor.innerText = item.author;

        // дата
        let listDate = document.createElement('h2');
        listDate.classList.add('date');
        listDate.innerText = item.createdAt;

        // зображення
        let image = new Image();
        image.src = item.imageUrl;
        let listImage = document.createElement('div')
        listImage.classList.add('image');
        listImage.appendChild(image);

        // текст
        let listText = document.createElement('p');
        listText.innerText = item.text;

        // лайки
        let listLikes = document.createElement('div');
        listLikes.classList.add('likes');
        listLikes.innerText = `Likes: ${item.likes}`;
        listLikes.addEventListener('click', item.AddLike.bind(item));

        // коментарі
        let listComments = document.createElement('div');
        listComments.classList.add('comments-info');
        listComments.dataset.id = item.id;
        let arrayComments = item.comments;
        listComments.innerText = `Comments (${arrayComments.length})`;

        // listComments.addEventListener('click', item.ShowComments.bind(item));

        // коментарі
        let addComment = document.createElement('ul');
        addComment.classList.add('list-comments');
        addComment.dataset.id = item.id;

        if (arrayComments.length > 0) {
            arrayComments.forEach(function(item) {
                if (item != null) {
                    let list = document.createElement('li');
                    let author = document.createElement('h3');
                    author.innerText = item.author;
                    list.appendChild(author);

                    let date = document.createElement('h3');
                    date.classList.add('date');
                    date.innerText = item.createdAt;
                    list.appendChild(date);

                    let text = document.createElement('p');
                    text.innerText = item.text;
                    list.appendChild(text);

                    addComment.appendChild(list);
                }
            })
        }

        // додати коментар
        let listCommentBtn = document.createElement('button');
        listCommentBtn.innerText = 'Write comment';
        listCommentBtn.dataset.id = item.id;
        listCommentBtn.addEventListener('click', item.WriteComment.bind(item));

        // додаємо елементи в ul
        listPost.appendChild(listAuthor);
        listPost.appendChild(listDate);
        listPost.appendChild(listImage);
        listPost.appendChild(listText);
        listPost.appendChild(listLikes);
        listPost.appendChild(listCommentBtn);
        listPost.appendChild(listComments);
        listPost.appendChild(addComment);

        newsList.appendChild(listPost);
        addId++;
    })
}


// констурктор коментаря
class Comment extends Post {
    constructor(Id, Author, Text) {
        super(Id, Author, Text);
    };
};

// забераємо з сховища в локаний об'єкт
let addId = 0;
let localObject = localStorage.getItem('newsPosts');
if (localObject !== null) {
    // console.log(localObject);
    localObject = JSON.parse(localObject);
    localObject.forEach(function(item) {
        let likes = item.likes;
        let date = item.createdAt;
        let comments = item.comments;

        item = new Post(item.id, item.author, item.text, item.imageUrl);
        item.likes = likes;
        item.createdAt = date;
        item.comments = comments;

        postObj.push(item);
    });

    CreatePosts();
    postObj.reverse();
}


// отримання елементів
let addPost = document.getElementById('postForm');
let addAutor = document.querySelector('input[name=Author]');
let addText = document.querySelector('textarea[name=Text]');
let postButton = document.getElementById('sendPost');
let addImage = document.querySelector('input[name=image-url]');
addImage.addEventListener("change", function() {
    if (!validUrl(addImage.value)) {
        addImage.setCustomValidity('URL must start with http or https');
    } else {
        addImage.setCustomValidity('');
        document.getElementById('imagePreview').src = addImage.value;
    }

    function validUrl(item) {
        let regExp = /^(http|https):\/\//i;
        return regExp.test(String(item).toLowerCase());
    }
});

// обробник форми
addPost.addEventListener('submit', function(e) {
    e.preventDefault();

    let newPost = new Post(addId, addAutor.value, addText.value, addImage.value);
    postObj.push(newPost);

    refreshLocalStorage();

    CreatePosts();
    addId++;
    addAutor.value = null;
    addText.value = null;
    addImage.value = null;
    postObj.reverse();
});