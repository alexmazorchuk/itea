  /*
    Задание:
    Скопировать текст из файла RegExp.txt на сайт https://regexr.com/
    Написать регулярное выражение которое найдет:
      1. Все слова. 
      2. Все совпадения букв от a до e,
      3. Года, например 2004
      4. Слова обернутые в [квадратныеКавычки]
      5. Все форматы файлов .jpg / .png / .gif
      6. Все email com.ua
      7. Все слова написанные с большой буквы
      8. Найти телефон написаный по паттерну +00 (000) 000-00-00, 
         где вместо 0 может быть любое число
  */

  /*
  Приклад для перевірки:
  Lorem ae ipsum is a [pseudo] Latin [] texte eused in web design, typography, layout, and printing in place of English to emphasise design elements over content. It's also called placeholder repeat (or filler) text. It's a convenient tool for http://facebook.com mock-ups vasya@gmail.com. It helps to outline the visual elements of a document or presentation.gif, eg typography.GIF, font, or layout. Lorem ipsum is mostly a part of a Latin text by the classical author gray and color colour philosopher Cicero. Its words and he letters 2006 have been changed by addition or removal, so to deliberately render its content nonsensical; it's not genuine, grey correct, or comprehensible Latin anymore. While lorem.jpg ipsum's [Sstill] resembles classical Latin, it actually has no meaning whatsoever. As Cicero's text doesn't contain the letters K, W, or Z, alien to latin, these, and others are often inserted randomly to mimic the typographic appearence of European languages, as are andrew@itea.ua digraphs not to be found in the original. 1987.png csdfdsfsdfs@com.ua 019aaa@com.ua aaa019@com.ua aaaa.aa@com.ua aaa_maz@com.ua +00 (000) 000-00-00 +12 (123) 222-22-22

  Регулярки
  1. ([a-zA-Z]){3}\w+ -gi
  2. (([a-e]){0,} -gi
  3. [1-9]{1}[0-9]{3} -g
  4. (\[[a-z])\w+\] -gi
  5. \.(png|jpg|gif) -gi
  6. ([a-zA-Z0-9\.\_]*)@(com\.ua) -g
  7. ([A-Z]){1}\w+ -g
  8. \+[0-9]{2}\s\([0-9]{3}\)\s[0-9]{3}(\-[0-9]{2}){2}

  */