/*

    Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678


*/

let myObj = [];

// констуртор для користувача
class User {
  constructor(login, pass){
    this.login = login;
    this.pass = pass;

    myObj.push(this);
  }
}

// перевірка длкального сховища
let localUsers = localStorage.getItem('users');
if (localUsers !== null) {
    // повідомлення якщо є
    alert('Hello ' + JSON.parse(localUsers).login ) 

} else {
    // інакше додаємо inputs
    // ??? можливо краще буде через render()
    let login = document.createElement('input');
        login.type = 'text';
        login.name = 'login';
        login.placeholder = 'Input our logon'
    document.body.appendChild(login);

    // input для пароля
    let pass = document.createElement('input');
        pass.type = 'password';
        pass.name = 'pass';
    document.body.appendChild(pass);

    // input кнопки
    let btn = document.createElement('input');
        btn.setAttribute('id', 'ident');
        btn.type = 'button';
        btn.value = 'Send';
        btn.addEventListener('click', function() {
            let log = document.querySelector('input[name=login]').value;
            let pas = document.querySelector('input[name=pass]').value;

            myObj = new User(log, pas);
            localStorage.setItem('users', JSON.stringify(myObj) );
            btn.disabled = true; 
        })
    document.body.appendChild(btn);

}

  





