/*

    Задание 3:


    Написать класс Posts в котором есть данные:

    _id
    isActive,
    title,
    about,
    likes,
    created_at

    У класса должен быть метод addLike и render.

    Нужно сделать так чтобы:
    - После добавления поста, данные о нем записываются в localStorage.
    - После перезагрузки страницы, данные должны сохраниться.
    - Можно было предзагрузить данные в данный модуль: http://www.json-generator.com/api/json/get/cgCRXqNTtu?indent=2

*/
let myObj = [];

// додаємо класс 
class Posts {
    constructor ( _id, isActive, title, about, likes, createdAt ) {
        this._id = _id;
        this.isActive = isActive;
        this.title = title;
        this.about = about;
        this.likes = likes;
        this.createdAt = createdAt;
        
        // обробка властивості додання like
        this.addLike = this.addLike.bind(this);

        myObj.push(this);
    }

    addLike() {
        this.likes++;
        let addLikes = document.querySelector(`[data-id="${this._id}"] .likes`);
            addLikes.innerHTML = this.likes;

        // зберігаємо лайки!!!!!
        saveMyObj();

    }

    render() {
        let result = document.createElement('div');
            result.dataset.id = this._id;
            result.innerHTML = `
                <hr>
                <h2> ${this.title}</h2>
                <p> ${this.about}</p>
                <p> Active: ${this.isActive}</p>
                <p class='likes'>${this.likes}</p>
                <button class='addLike'>Add like</button>
                <p> Created: ${this.createdAt}</p>
            `
        let btn = result.querySelector('.addLike');
            btn.addEventListener('click', this.addLike);
        document.body.appendChild(result);
    }
}

// будуємо HTML
//  додаємо inputs для title
let title = document.createElement('input');
    title.type = 'text';
    title.name = 'title';
    title.placeholder = 'Title';
    title.style.display = 'block';
document.body.appendChild(title);

// input для about
let about = document.createElement('input');
    about.type = 'text';
    about.name = 'about';
    about.placeholder = 'About...';
    about.style.display = 'block';
document.body.appendChild(about);

// lable для checkbox
let lable = document.createElement('label');
    lable.innerText = 'Is active';
    lable.style.display = 'block';
document.body.appendChild(lable);

// checkbox для статусу
let active = document.createElement('input');
    active.type = 'checkbox';
    active.name = 'active';
lable.appendChild(active);

// input кнопки додання
let btn = document.createElement('input');
    btn.setAttribute('id', 'post');
    btn.type = 'button';
    btn.value = 'Post';
    btn.addEventListener('click', function() {
        let id = Number(new Date());
        let active = document.querySelector('input[name=active]:checked') ? true : false;
        let title = document.querySelector('input[name=title]').value;
        let about = document.querySelector('input[name=about]').value;
        let likes = 0;
        let createdAt = new Date();
        if (title && about ) {
            new Posts(id, active, title, about, likes, createdAt ).render();
            saveMyObj();

        } else { 
            alert('Some fields empty') 
        }
    })
document.body.appendChild(btn);

// input кнопки завантаження з url
let btn2 = document.createElement('input');
    btn2.setAttribute('id', 'load');
    btn2.type = 'button';
    btn2.value = 'Load';
    btn2.addEventListener('click', function() {
        fetch('http://www.json-generator.com/api/json/get/cgCRXqNTtu?indent=2')
        .then((res) => {
                return res.json()
            }
        )
        .then((data) => {
            console.log(data)
            // data.map(el => {
            //     new Posts(el._id, el.isActive, el.title, el.about, like, el.created_at).render();
            //     saveMyObj();
            // })
            data.forEach((el) => {
                new Posts(el._id, el.isActive, el.title, el.about, 0, el.created_at).render();
                saveMyObj();
            });
        })
    })
document.body.appendChild(btn2);

// ф-я для збереження в локальне сховище
let saveMyObj = () => localStorage.setItem('posts', JSON.stringify(myObj));

// перевірка локального сховища, якщо не пусте, то відвантажуємо елементи до html
let localPosts = localStorage.getItem('posts');
if (localPosts !== null) {
    // console.log(JSON.parse(localPosts).map(el => console.log(el)))
    JSON.parse(localPosts).map(i => new Posts(i._id, i.isActive, i.title, i.about, i.likes, i.createdAt).render())
    
} else {
    myObj = []
}