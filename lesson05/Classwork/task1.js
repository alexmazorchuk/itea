/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/

var objTrain = {};
    objTrain.name = '001A';
    objTrain.speed = 130;
    objTrain.passengers = 90;
    objTrain.go = function() {
        console.log('Поезд ', this.name, ' везет ', this.passengers,  ' пассажиров со скоростью ', this.speed);
    };
    objTrain.standing = function() {
        console.log('Поезд ' + this.name + ' остановился. Скорость ' + this.speed);
    };
    objTrain.pickUp = function() {
        console.log('Увеличивает кол-во пассажиров на X');
    };