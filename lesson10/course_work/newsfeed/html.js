// ************* головний блок *************
let main = document.createElement('div');
// main.setAttribute('id', 'main');

let body = document.querySelector('body');
body.appendChild(main);


// ************* блок новин *************
let news = document.createElement('div');
// news.setAttribute('id', 'news');
news.classList.add('main');
main.appendChild(news);

// заголовок
let titleNews = document.createElement('h1');
titleNews.innerText = 'News Feed';
news.appendChild(titleNews);

// список новин
let list = document.createElement('ul');
list.setAttribute('id', 'newsList');
news.appendChild(list);


// ************* блок додання *********
let post = document.createElement('div');
// post.setAttribute('id', 'post');
post.classList.add('main');
main.appendChild(post);

// заголовок
let titlePost = document.createElement('h1');
titlePost.innerText = 'Post Constructor';
post.appendChild(titlePost);

// форма
let form = document.createElement('form');
form.setAttribute('id', 'postForm');
post.appendChild(form);

// автор
let labAuthor = document.createElement('label');
labAuthor.innerText = 'Author';
form.appendChild(labAuthor);

// поле вводу автора
let postAuthor = document.createElement('input');
postAuthor.type = 'text';
postAuthor.name = 'Author';
postAuthor.required = 'true';
form.appendChild(postAuthor);

// текст
let labText = document.createElement('label');
labText.innerText = 'Text';
form.appendChild(labText);

// поле тексту
let postSomeText = document.createElement('textarea');
postSomeText.type = 'text';
postSomeText.rows = 5;
postSomeText.name = 'Text';
postSomeText.required = 'true';
form.appendChild(postSomeText);

// url картинки
let labUrl = document.createElement('label');
labUrl.innerText = 'Image URL';
form.appendChild(labUrl);

// поле вводу урл
let imageUrl = document.createElement('input');
imageUrl.type = 'text';
imageUrl.name = 'image-url';
form.appendChild(imageUrl);

// картинка
let imageText = document.createElement('label');
imageText.innerText = '';
form.appendChild(imageText);

// елемент прев'ю картинки
let imagePrev = document.createElement('div');
imagePrev.classList.add('image-preview');
form.appendChild(imagePrev);
let image = document.createElement('img');
image.setAttribute('id', 'imagePreview');
image.src = "./image_default.svg"
imagePrev.appendChild(image);

// кнопка доданння
let postBtn = document.createElement('button');
postBtn.innerText = 'Send Post';
postBtn.classList.add('send-btn');
postBtn.setAttribute('id', 'sendPost');
form.appendChild(postBtn);