/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/

// 1, 2, 3
let url = 'http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2';
let url2 = 'http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2';
let arrayNames = [];
function NewObject(name, friends) {
  this.name = name;
  this.friends = friends;
}

const testFunc = (res) => {
  // console.log('Response', res);
  return res.json();
}
const test2Func = (res) => {
  res.forEach( item => {
    arrayNames.push(item.name);
    console.log('Item:', item);
  })
  return arrayNames;
}
const test3Func = (names) => {
  let max = (names.length -1);
  let index = getRandomIntInclusive(0, max);
  let name = names[index];
  console.log('Name:', name);
  return name;
}
function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const friendsResponse = (json, name) => {
  console.log('name', name)
  console.log(json)
}
fetch( url )
  .then( testFunc )
  .then( test2Func )
  .then( test3Func )
  .then( name => { // ??? як мені name передати у функцію вгорі?
    return fetch(url2)
    .then( testFunc )
    .then( 
      (json) => {
        // friendsResponse
        let obj = new NewObject(name, json[0].friends)
        // console.log(name);
        // console.log(json[0].friends)
        console.log(obj);
        let div = document.createElement('div');
            div.innerText = obj;
        let body = document.querySelector('body');
            body.append(JSON.stringify(obj));
      }
    )
  })

