/*
  Задание:

  Написать класс SuperDude который как аргумент принимает два параметра:
    - Имя
    - Массив суперспособностей которые являются обьектом.

    Модель суперспособности:
      {
        // Имя способности
        name:'Invisibility',
        // Сообщение которое будет выведено когда способность была вызвана
        spell: function(){ return `${this.name} hide from you`}
      }

    В конструкторе, нужно:
    - сделать так, что бы имя нельзя было перезаписывать и присвоить ему то
      значение которое мы передали как аргумент.

    - перебрать массив способностей и на каждую из них создать метод для этого
      обьекта, используя поле name как название метода, а spell как то,
      что нужно вернуть в console.log при вызове этого метода.
    - все способности должны быть неизменяемые

    - бонус, создать конструктор суперспособностей -> new Spell( name, spellFunc );
*/

// створюємо класс,
// чеерз конструткор приймає два парамтера: name, superPower як масив
  class SuperDude {
    constructor( name, superPowers = [] ) {
      // через defineproperty призначаємо 'name', щоб не змінювалося
      Object.defineProperty(
        this,
        'name',
        {
          value: name,
        }
      );
      
      // циклом по масиву, щоб визначити кожен об'єкт масиву
      superPowers.forEach( power => {
        // також через властивість, щоб не змінювалася
        Object.defineProperty(
          this,
          power.name,
          {
            value: () => { console.log('Power spell', power.spell()) }
          }
        )
      });
    }
  }

  let superPowers = [
    { name:'Invisibility', spell: function(){ return `${this.name} hide from you`} },
    { name:'superSpeed', spell: function(){ return `${this.name} running from you`} },
    { name:'superSight', spell: function(){ return `${this.name} see you`} },
    { name:'superFroze', spell: function(){ return `${this.name} will froze you`} },
    { name:'superSkin',  spell: function(){ return `${this.name} skin is unbreakable`} },
  ];

  // додаємо новий клас Spell 
  // присвоюємо властивість з name, а значення з function 
  class Spell {
    constructor(name, spellFunc) {
      Object.defineProperty(
        this,
        name,
        {
          value: () => { console.log('Test spell', spellFunc()) },
      });

    }
  }

  //  перевіряємо функцію констуртор
  // ?? не вийшло прокинути this до функції
  let newFunc = new Spell('testFunc', function(){ return 'test it'});
      newFunc.testFunc();

  // задаємо новий об'єкт не основі класа SuperDude
  let Luther = new SuperDude('Luther', superPowers);
      // Тестирование: Методы должны работать и выводить сообщение.
      Luther.superSight();
      Luther.superSpeed();
      Luther.superFroze();
      Luther.Invisibility();
      Luther.superSkin();

