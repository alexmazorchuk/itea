/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>


*/

// 1. Функция-конструктор для создания объекта Comment
function Comment(name, text, avatarUrl) {
    this.name = name;
    this.text = text;
    if (avatarUrl) {
      this.avatarUrl = avatarUrl;
    }
    this.likes = 0;
}

// прототип фунції з картинкою за замовчуванням і лічильником like
Comment.prototype = {
  avatarUrl: 'https://image.flaticon.com/icons/png/512/147/147144.png',
  addLike: function(){
    this.likes++;
  }
}

// 2. масив 4-х коментарів
let myComment1 = new Comment('name1', 'first commetn');
let myComment2 = new Comment('name2', 'second commetn', 'https://image.flaticon.com/icons/png/512/194/194938.png');
let myComment3 = new Comment('name3', 'lalala commetn', 'https://cdn.iconscout.com/icon/free/png-256/avatar-370-456322.png');
let myComment4 = new Comment('name4', 'ololol commetn');
let commentsArray = [myComment1, myComment2, myComment3, myComment4];

// 3. функція, що створить масив кометарів
let commentDiv = document.getElementById('CommentsFeed');
function addComments(array) {
  this.array = array;
  this.array.forEach(element => {
    // додоємо базовий div
    let newDiv = document.createElement('div');
    // додаємо заголовок
    let title = document.createElement('h1');
        title.innerText = element.name;
        newDiv.appendChild(title);
    // додаємо коментар
    let text = document.createElement('p');
        text.innerText = element.text;
        newDiv.appendChild(text);
    // додаємо аватарку
    let avatar = document.createElement('img');
        avatar.src = element.avatarUrl;
        avatar.style.maxWidth = '100px';
        newDiv.appendChild(avatar);
    // додаємо число like
    let like = document.createElement('div');
        like.className = 'like'
        like.innerText = element.likes;
        newDiv.appendChild(like);
    // додаємо подію для числа like
    newDiv.addEventListener('click', function() {
      element.addLike();
      newDiv.querySelector('div').innerHTML = element.likes;
    });
    commentDiv.appendChild(newDiv);
  });
}

addComments(commentsArray);
