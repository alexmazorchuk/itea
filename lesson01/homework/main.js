/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 16-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 255;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/

// 1. RGB body
function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
console.log('Page reload');
r = getRandomIntInclusive(0, 255);
g = getRandomIntInclusive(0, 255);
b = getRandomIntInclusive(0, 255);
console.log('color RGB: ', r, g, b);
document.body.style.background = 'rgb(' + r + ',' + g + ',' + b + ')';

// 2. BUTTON add
document.getElementById('app').innerHTML = '<button onclick="ChangeColor()">Chahge color</button>';

// 3. HEX body onclick btn
function ChangeColor() {
    console.log('Click btn');

    function getRandomIntInclusive(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    var r = getRandomIntInclusive(0, 255).toString(16);
    var g = getRandomIntInclusive(0, 255).toString(16);
    var b = getRandomIntInclusive(0, 255).toString(16);

    if (r.length == 1) {
        r = '0' + r;
    }
    if (g.length == 1) {
        g = '0' + g;
    }
    if (b.length == 1) {
        b = '0' + b;
    }
    dataColor = document.body.style.background = '#' + r + g + b;

    console.log('color HEX: ', dataColor);

    // 4. HEX update
    var text = document.getElementById('color');
    text.innerText = dataColor;
}

// 4. HEX output
var color = document.createElement('div');
color.id = 'color';
color.innerText = 'RGB(' + r + ',' + g + ',' + b + ')';
document.getElementById('app').appendChild(color).style.cssText = 'position:fixed;left:50%;top:50%;transform:translate(-50%,-50%);';