/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.


    const isMobile = document.innerWidth < 768;

    Например:
      encryptCesar( 3, 'Word');
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1(3, 'Sdwq');
      decryptCesar1(...)
      ...
      decryptCesar5(...)

      "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾

*/

// додав HTML щоб виводити результат на веб-сторінку
let inputValue = document.createElement('input');
    inputValue.setAttribute('id', 'encode');
document.body.appendChild(inputValue);

let shiftValue = document.createElement('input');
    shiftValue.setAttribute('id', 'key');
    shiftValue.setAttribute('type', 'number');
document.body.appendChild(shiftValue);

let encodeBtn = document.createElement('button');
    encodeBtn.setAttribute('id', 'to-encode');
    encodeBtn.innerText = 'Encode'
document.body.appendChild(encodeBtn);

let btn = document.getElementById('to-encode');
btn.addEventListener('click', function() {
  let inputText = document.getElementById('encode').value;
  let shift = document.getElementById('key').value;

  encryptCesar(shift,inputText);
});

// функція кодування
function encryptCesar(shift, inputText) {
  console.log('inputText.length:', inputText.length);
  console.log('shift:', shift);

  if (shift > 0 || inputText.length > 1) {
    let result = '';
    for (let i = 0, len = inputText.length; i < len; i++) {
      let a = inputText[i].charCodeAt(0);
      var n = Number(shift)

      if (a >= 65 && a <= 90){ // для літер A-Z
        var e =  (((a - 65 + n) % 26) + 65);
      }
      else if (a >= 97 && a <= 122){ // для літер a-z
        var e = (((a - 97 + n) % 26) + 97);
      }
      else if (a >= 1040 && a <= 1071){ // для А-Я
        var e = (((a - 1040 + n) % 32) + 1040);
      }
      else if (a >= 1072 && a <= 1103){ // для а-я
        var e = (((a - 1072 + n) % 32) + 1072);
      }
      else {
        var e = inputText[i];
      }
      result = result + String.fromCharCode(e);
    }
    // виводимо результат на сторінку
    let resDiv = document.createElement('div');
    resDiv.innerText = result ;
    document.body.appendChild(resDiv);
  }
}

// використовуємо bind для кодування з іншим кроком
let encryptCesar5 = encryptCesar.bind(null, 5);
console.log(encryptCesar5('Cesar'));


// функція декодування
// все те ж саме, тільки у формулі віднімаємо значення shift
function decryptCesar(shift, inputText) {
  console.log('inputText.length:', inputText.length);
  console.log('shift:', shift);

  if (shift > 0 || inputText.length > 1) {
    let result = '';
    for (let i = 0, len = inputText.length; i < len; i++) {
      let a = inputText[i].charCodeAt(0);
      var n = Number(shift)

      if (a >= 65 && a <= 90){ // для літер A-Z
        var e =  (((a - 65 - n) % 26) + 65);
      }
      else if (a >= 97 && a <= 122){ // для літер a-z
        var e = (((a - 97 - n) % 26) + 97);
      }
      else if (a >= 1040 && a <= 1071){ // для А-Я
        var e = (((a - 1040 - n) % 32) + 1040);
      }
      else if (a >= 1072 && a <= 1103){ // для а-я
        var e = (((a - 1072 - n) % 32) + 1072);
      }
      else {
        var e = inputText[i];
      }
      result = result + String.fromCharCode(e);
    }
    // виводимо результат на сторінку
    let resDiv = document.createElement('div');
    resDiv.innerText = result ;
    document.body.appendChild(resDiv);
  }
}

// використовуємо bind для декодування з іншим кроком
let decryptCesar5 = decryptCesar.bind(null, 5);
console.log(decryptCesar5('Hjxfw'));