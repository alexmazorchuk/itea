/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/

  var OurSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
  var currentPosition = 0;

  var buttons = document.querySelectorAll('button');
  var slideBlock = document.getElementById('slider');

  // 1. додаємо подію onload об`єкта window і 
  // завантажуємо ф-ю imageMain в яку передаємо число
  window.addEventListener('load', function(){
    imageMain(currentPosition);
  });

  // функція додання картинки на сторінку по числу з лічильника currentPosition
  function imageMain(el) {
    // console.log(currentPosition);
    // console.log(OurSliderImages.length);

    let image1 = new Image(500);
    image1.onload = function(){
      slideBlock.appendChild(image1);
      image1.classList.add('animation');
    };
    image1.onerror = function(err){
      new Error('Some Error on load', err);
    };
    image1.src = OurSliderImages[el];
  }

  // на кнопки Next/Prev додаємо властивість click і 
  // передаємо в функцію RenderImage
  buttons.forEach( (btn) => {
    btn.addEventListener('click', RenderImage);
  });

  // функція RenderImage
  function RenderImage(event) {
    // console.log(event);
    // визначаємо id натиснутої кнопки і 
    // за умов додаємо або віднімаємо число currentPosition
    // можливо в окремі ф-її було нада виділити??
    let buttonClick = event.target.id;
    if (buttonClick == 'NextSilde') {
      currentPosition++
    }
    if (buttonClick == 'PrevSilde') {
      currentPosition--
    }

    // умова перевірки числа на нійбільш допустиме=length(масив картинок) і найменше=0
    if (currentPosition == OurSliderImages.length) {
      currentPosition = 0;
    }
    if (currentPosition < 0) {
      currentPosition = 7;
    }
    // видаляємо картинку, щоб мо ф-ї imageMain додати нову
    let image1 = slideBlock.querySelector('img');
    image1.remove();

    imageMain(currentPosition);
  }
