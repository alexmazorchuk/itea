
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */

// визначили батьківський елемент всх кнопок
var button1 = document.getElementById('buttonContainer');
// додаємо подію onclick
button1.onclick = function(event) {
  console.log('click', event);
  // визначаємо значення data-tab кнопки
  let tab = event.target.dataset.tab;
  console.log('tab: ', tab);
  // передаємо до функції, яка додаватиме active потрібному tab
  active(tab);
}

// функція додаватиме клас active для data-tab елемента
function active(element) {
  // додаємо функцію яка видалятиме клас active з раніше встановвленого ел-а
  hideAllTabs();
  // визначаємо потрібний tab та додаємл класс active
  let tab = document.querySelector('.tab[data-tab="'+element+'"]')
  tab.classList.add('active');
}

// функція видалення класу active з дочірніх ел-ів tabContainer
function hideAllTabs() {
  // визначаємо нащадків батьківського елементу tabContainer
  let mainDiv = document.querySelector('#tabContainer').children;
  console.log(mainDiv);
  // зводимо в масив, по якому проходимося циклом, щоб видалити клас active
  Array.from(mainDiv).forEach( item => {
    item.classList.remove('active');
    // console.log(item);
  })
}
