/*

    Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

*/

// створюємо input для кольору й подію
let btn = document.createElement('input');
    btn.setAttribute('id', 'setcolor');
    btn.type = 'button';
    btn.value = 'Set color';
    btn.addEventListener('click', function() {
        let color = getMyColor();
        localStorage.setItem('back', color);
        document.body.style.background = color;
    })

document.body.appendChild(btn);

// функція випадкових чисел
function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// функція отримання випадкового кольору HEX
function getMyColor() {
    let r = getRandomIntInclusive(0, 255).toString(16);
    let g = getRandomIntInclusive(0, 255).toString(16);
    let b = getRandomIntInclusive(0, 255).toString(16);

    r.length == 1 ? r = ('0' + r) : r;
    g.length == 1 ? g = '0' + g : g;
    b.length == 1 ? b = '0' + b : b;
    return '#' + r + g + b;
    // console.log('color HEX: ', hexColor);
}