/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/

function NewObject(name, balance, date, address) {
  this.Company = name;
  this.Balance = balance;
  this.Registered = date;
  this.Address = address;
}
let arr = [];
async function getCompanyInfo(){
  let getResponse = await fetch("http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2")
  let users = await getResponse.json();
  console.log(users);

  users.forEach(el => {
    let obj = new NewObject(el.company, el.balance, el.registered, el.address);
    arr.push(obj);
  });
  // console.log('obj:', arr);

  return arr;
}

// створюємо div елементи з властивостями об'єкта
// та додаємо кнопки input за якими будемо show/hode текст 
let companyInfo = getCompanyInfo();
companyInfo.then( data => {
  console.log('Data:', data);
  Array.from(data).forEach(item => {
    let elem = document.createElement('div');
      elem.innerHTML = `
      <div class='index'>${(data.indexOf(item) + 1)}</div>
      <div class='name'>${item.Company}</div>
      <div class='bal'>${item.Balance}</div>
      <input type="button" id="hidden" value="Date" />
      <div class='date' hidden>${item.Registered}</div>
      <input type="button" id="hidden" value="Address" />
      <div class='address' hidden>${JSON.stringify(item.Address)}</div>
      `
      let company = document.getElementById('company');
          company.appendChild(elem);
 
  });

  // додаємо подію onclick для всіх кнопок з id=hidden
  var hidden = document.querySelectorAll("#hidden");
  for (var i = 0; i < hidden.length; i++) {
    hidden[i].onclick = function(el){
      if (el.target.nextElementSibling.hidden) {
        el.target.nextElementSibling.hidden = false;
      } else {
        el.target.nextElementSibling.hidden = true;
      }
    };
  }
});

