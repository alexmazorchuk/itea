
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>
    <button></button>
  </form>

  <form>
    <input />
    <button></button>
  </form>
  -> '{"name" : !"23123", "age": 15, "password": "*****" }'

*/
window.addEventListener('DOMContentLoaded', function() {

  // 1.
  let btn = document.querySelector('button');
      btn.innerText = 'To string in console'
      btn.addEventListener('click', function(e){
        e.preventDefault();
        let form = document.querySelector('form');
        let name = form.elements.name.value;
        let age = form.elements.age.value;
        let pass = form.elements.password.value;
        let obj = {
          'name': name,
          'age': age,
          'pass': pass
        }
        // форматуємо обєкт в стрічку
        console.log(JSON.stringify(obj));
      })

  // 2  Додав через id=form2 в index.html
  let second = document.getElementById('form2');
  let btn2 = second.querySelector('button');
      btn2.innerText = 'To Json from object';
      btn2.addEventListener('click', function(e){
        e.preventDefault();
        let input = second.querySelector('input').value;
        let result = JSON.parse(input);
        // форматуємо стрічку в обєкт
        console.log(result);
      })
  console.log(btn2)

})

